package testcases;

import java.io.IOException;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebElement;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import wdMethods.SeMethods;
import week6.day2.LearnExcel;
@Test 
public class CreateLead extends ProjectMethods{

	//@Test(invocationCount = 2)
	//@Test(timeOut = 15000)
	//@Test(enabled = false)
//	@Test(groups = {"smoke"})
@DataProvider(name = "qa", indices= {2})
	public Object[][] values() throws IOException {
	Object[][] data = LearnExcel.readExcel();
	//Object[][] data = new Object[2][3];
		/*data[0][0] = "HCL";
		data[0][1] = "Sai";
		data[0][2] = "kanna";
		
		data[1][0] = "Syntel";
		data[1][1] = "Giri";
		data[1][2] = "Giri";*/
		return data;
	}
	
	@Test(dataProvider = "qa")
	
	public void createLead(String cname, String fname, String lname, String emailId, String ph) throws InterruptedException  {
//	public void createLead() throws InterruptedException  {
		WebElement createLead = locateElement("linktext","Create Lead");
		click(createLead);
		WebElement companyName = locateElement("id","createLeadForm_companyName");
		type(companyName,cname);
		WebElement firstName = locateElement("id","createLeadForm_firstName");
		type(firstName,fname);
		WebElement lastName = locateElement("id","createLeadForm_lastName");
		type(lastName,lname);
		WebElement email = locateElement("id","createLeadForm_primaryEmail");
		type(email,emailId);
		WebElement phone = locateElement("id","createLeadForm_primaryPhoneNumber");
		type(phone, ph);
		WebElement submitLead = locateElement("name","submitButton");
		click(submitLead);
		/*WebElement mergeLead = locateElement("linktext","Merge Leads");
		click(mergeLead);
		WebElement fromLead = locateElement("xpath","(//img[@alt='Lookup'])[1]");
		click(fromLead);
		//From Lead Window
		switchToWindow(1);
		WebElement findLeads = locateElement("xpath","//button[text()='Find Leads']");
		click(findLeads);
		Thread.sleep(2000);
		WebElement eleFirstName = locateElement("name","firstName");
		Thread.sleep(2000);
		type(eleFirstName,"sa");
		Thread.sleep(2000);
		WebElement table = locateElement("xpath","//*[@id=\'ext-gen253\']/div[1]/table");
		Thread.sleep(2000);
		List<WebElement> rows = table.findElements(By.tagName("tr"));
		int size = rows.size();
		System.out.println(size);
		for (WebElement row : rows) {
			System.out.println(row);	
		}
		
		
		List<WebElement> cols = rows.get(1).findElements(By.tagName("td"));
		Thread.sleep(3000);
		String text1 = cols.get(1).getText();
		System.out.println(text1);
		WebElement select1 = locateElement("linktext",text1);
		clickWithNoSnap(select1);
		List<WebElement> rows = table.findElements(By.tagName("tr"));
		System.out.println(rows.size());
		//To Lead window
		Thread.sleep(3000);
		switchToWindow(0);
		Thread.sleep(3000);
		WebElement toLead = locateElement("xpath","(//img[@alt='Lookup'])[2]");
		Thread.sleep(3000);
		click(toLead);
		Thread.sleep(3000);
		switchToWindow(1);
		WebElement eleFirstName1 = locateElement("name","firstName");
		type(eleFirstName1,"ra");
		WebElement findLeads1 = locateElement("xpath","//button[text()='Find Leads']");
		click(findLeads1);
		Thread.sleep(3000);
		WebElement select2 = locateElement("linktext","10035");
		clickWithNoSnap(select2);
		*/
		closeBrowser();
	}
	
}












