package testcases;
import java.util.Set;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;
public class MergeLead extends ProjectMethods {
	//@Test(dependsOnMethods = {"testcases.CreateLead.createLead"}, alwaysRun = true)
	//@Test(groups = {"sanity"}, dependsOnGroups = {"smoke"})
	@Test(groups = {"sanity"})
	public void mergeLead() throws InterruptedException {
		
			WebElement leadLinkClick = locateElement("linktext","Create Lead");
		
			click(leadLinkClick);
		   WebElement mergeLeadClick = locateElement("linktext","Merge Leads");
			
			click(mergeLeadClick);
			
			WebElement FromLead = locateElement("xpath","//input[@name='partyIdFrom']//following::a");
			
			click(FromLead);
			
			switchToWindow(1);
			
			WebElement FirstName=locateElement("name","firstName");
			type(FirstName, "Koushik");
			
			WebElement FindLead = locateElement("xpath","//button[text()='Find Leads']");
			
			click(FindLead);
			
			WebElement FindLink = locateElement("xpath","(//div[@class='x-grid3-cell-inner x-grid3-col-firstName'])[2]/a");
			
			clickWithNoSnap(FindLink);
			
			switchToWindow(0);
			//Second image to merge
		    
			WebElement ToLead = locateElement("xpath","//input[@name='ComboBox_partyIdTo']//following::a/img");
			click(ToLead);
			switchToWindow(1);
			WebElement FirstName1=locateElement("name","firstName");
			type(FirstName1, "sam");
			
			WebElement FindLead1 = locateElement("xpath","//button[text()='Find Leads']");
			
			click(FindLead1);
			
			WebElement FindLink1 = locateElement("xpath","(//div[@class='x-grid3-cell-inner x-grid3-col-firstName'])[2]/a");
			//Thread.sleep(3000);
			clickWithNoSnap(FindLink1);
			//Thread.sleep(3000);
			switchToWindow(0);
			//Thread.sleep(3000);
			WebElement MergeLead=locateElement("class","buttonDangerous");
			clickWithNoSnap(MergeLead);
			acceptAlert();
			
		
	}
}
